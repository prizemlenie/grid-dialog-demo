define([
	'dojo/_base/declare',
	'dojo/store/Memory',
	'dojo/data/ObjectStore',
	'dijit/form/TextBox',
	'dijit/form/Button',
	'dijit/layout/BorderContainer',
	'dijit/_OnDijitClickMixin',
	'dijit/_WidgetBase',
	'dijit/Dialog',
	'dojox/grid/DataGrid',
	'dijit/_TemplatedMixin',
	'dijit/_WidgetsInTemplateMixin',
	'dijit/focus',
	'dojo/text!/app/templates/GridDialog.html'	
], function(declare, Memory, ObjectStore, BorderContainer, TextBox, Button, _OnDijitClickMixin, _WidgetBase, Dialog, DataGrid, _TemplatedMixin, _WidgetsInTemplateMixin, focusUtil, template) {

	var DialogContent = declare([_WidgetBase, _OnDijitClickMixin, _TemplatedMixin, _WidgetsInTemplateMixin], {
		templateString: template,

		constructor: function() {
			this._objectStore = new Memory({
				data: [
					{firstName: 'Иван', secondName: 'Иванович', lastName: 'Иванов'},
					{firstName: 'Петр', secondName: 'Петрович', lastName: 'Петров'}
				]
			});
		},

		postCreate: function() {			
			this.inherited(arguments);			
			this._gridWidget.setStore(ObjectStore({objectStore: this._objectStore}));
		},

		_onAddClick: function(e) {
			var data = {
				firstName: this.firstNameField.value || 'Не указано',
				secondName: this.secondNameField.value || 'Не указано',
				lastName: this.lastNameField.value || 'Не указано',
			};

			this.firstNameField.set('value', '');
			this.secondNameField.set('value', '');
			this.lastNameField.set('value', '');
			focusUtil.focus(this.firstNameField);

			this._gridWidget.store.newItem(data); 
		}
	});

	var GridDialog = declare('app.GridDialog', [Dialog], {		
		constructor: function() {
			this.content = new DialogContent();			
		},

		startup: function() {
			this.inherited(arguments);
			var _this = this;

			// yep, it's dirty hack, but I have no enough time to investigate grid size issue further
			// issue description: 
			// if you try to add grid to dialog box immediately after dialog creation
			// grid height will be set to 0 or 1 px
			setTimeout(function() {_this.content._gridWidget.resize()}, 0);
		}
	});

	app.GridDialog._delBtnFormatter = function(item) {
		var _this = this,
			w;
		require(['dijit/form/Button',  'dojo/domReady!'],
			function(Button) {
				w = new Button({
					label: 'Удалить',
					onClick: function() {		        		
						_this.grid.store.deleteItem(item);
					}
				});
				w._destroyOnRemove=true;
			}
		);
		return w;
	};

	return GridDialog; 
});